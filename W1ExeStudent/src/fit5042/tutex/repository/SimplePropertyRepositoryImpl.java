/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.tutex.repository;

import fit5042.tutex.repository.entities.Property;
import java.util.ArrayList;
import java.util.List;

/**
 * TODO Exercise 1.3 Step 2 Complete this class.
 * 
 * This class implements the PropertyRepository class. You will need to add the keyword
 * "implements" PropertyRepository. 
 * 
 * @author Junyang
 */
public class SimplePropertyRepositoryImpl implements PropertyRepository {
	
	List<Property> propertyList = new ArrayList();

    public SimplePropertyRepositoryImpl() {
    	propertyList = new ArrayList<>();
        
    }
    public void addProperty(Property property) throws Exception{
    	if (!this.propertyList.contains(property) && this.searchPropertyById(property.getId()) == null)
			propertyList.add(property);
    	else
    		System.out.println("This is already existed");
    		
    }
    
    public Property searchPropertyById(int id) throws Exception {
		for (Property tempProperty : propertyList)
			if (tempProperty.getId() == id)
				return tempProperty;

		return null;
	}
    
   public List<Property> getAllProperties() throws Exception {
	   
	   return propertyList;
   }
}
