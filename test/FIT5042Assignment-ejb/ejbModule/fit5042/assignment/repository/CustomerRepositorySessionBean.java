/**
 * 
 */
package fit5042.assignment.repository;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import fit5042.assignment.repository.entities.Customer;

/**
 * @author Administrator
 *
 */
@Stateless
public class CustomerRepositorySessionBean implements CustomerRepository {

	@PersistenceContext (unitName = "FIT5042Assignment-ejb")//unitName = persistence-unit in jebModule/META-INF/persistence.xml
	private EntityManager entityManager;
	
	@Override
	public List<Customer> getAllCustomers() throws Exception {
		return entityManager.createNamedQuery(Customer.GET_ALL_QUERY_NAME).getResultList();	
	}
	

}
