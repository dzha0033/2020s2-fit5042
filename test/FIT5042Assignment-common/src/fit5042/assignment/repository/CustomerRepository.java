package fit5042.assignment.repository;

import java.util.List;

import javax.ejb.Remote;

import fit5042.assignment.repository.entities.Customer;


@Remote
public interface CustomerRepository {
	
    public List<Customer> getAllCustomers() throws Exception;
}
