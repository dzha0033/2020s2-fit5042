package fit5042.assignment.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.assignment.repository.entities.Customer;

@RequestScoped
@Named("searchCustomer")
public class SearchCustomer {
	private Customer customer;
	AUSApplication app;

	/**
	 * @param customer
	 * @param ausApplication
	 */
	public SearchCustomer() {
		ELContext context = FacesContext.getCurrentInstance().getELContext();

		app = (AUSApplication) FacesContext.getCurrentInstance().getApplication().getELResolver().getValue(context,
				null, "ausApplication");

		app.updateCustomerList();
	}

	/**
	 * @return the customer
	 */
	public Customer getCustomer() {
		return customer;
	}

	/**
	 * @param customer the customer to set
	 */
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	/**
	 * @return the app
	 */
	public AUSApplication getApp() {
		return app;
	}

	/**
	 * @param app the app to set
	 */
	public void setApp(AUSApplication app) {
		this.app = app;
	}
	
    public void searchAll() {
        try {
            //return all properties from db via EJB
            app.searchAll();
        } catch (Exception ex) {

        }
        //showForm = true;
    }

}
