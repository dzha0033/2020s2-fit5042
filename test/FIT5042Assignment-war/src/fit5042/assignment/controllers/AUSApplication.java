package fit5042.assignment.controllers;

import java.util.ArrayList;

import javax.el.ELContext;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.assignment.mbeans.CustomerManagedBean;
import fit5042.assignment.repository.entities.Customer;



@Named(value = "ausApplication")
@ApplicationScoped
public class AUSApplication {
    @ManagedProperty(value = "#{customerManagedBean}")
    CustomerManagedBean customerManagedBean;
    
    private ArrayList<Customer> customers;
    
//    private boolean showForm = true;
//
//    public boolean isShowForm() {
//        return showForm;
//    }
    
    // Add some customer data from db to app 
    public AUSApplication() throws Exception {
    	customers = new ArrayList<>();

        //instantiate customerManagedBean
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        customerManagedBean = (CustomerManagedBean) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(elContext, null, "customerManagedBean");

        //get properties from db 
        updateCustomerList();
    }
    
    public void updateCustomerList() {
//        if (customers != null && customers.size() > 0)
//        {
//            
//        }
//        else
//        {
        	customers.clear();

            for (Customer customer : customerManagedBean.getAllCustomers())
            {
            	customers.add(customer);
            }

            setCustomers(customers);
        //}
    }

	/**
	 * @return the customers
	 */
	public ArrayList<Customer> getCustomers() {
		updateCustomerList();
		// do more, access managedbean
		return customers;
	}

	/**
	 * @param customers the customers to set
	 */
	public void setCustomers(ArrayList<Customer> customers) {
		this.customers = customers;
	}
    
    public void searchAll()
    {
    	customers.clear();
        
        for (Customer customer : customerManagedBean.getAllCustomers())
        {
        	customers.add(customer);
        }
        
        setCustomers(customers);
    }
    
}
