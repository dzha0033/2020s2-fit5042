package fit5042.assignment.mbeans;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import fit5042.assignment.repository.CustomerRepository;
import fit5042.assignment.repository.entities.Customer;


@ManagedBean(name = "customerManagedBean")
@SessionScoped
public class CustomerManagedBean implements Serializable{
	
    @EJB
    CustomerRepository customerRepository;
    
    public CustomerManagedBean() {
    }
	
    public List<Customer> getAllCustomers() {
        try {
            List<Customer> customers = customerRepository.getAllCustomers();
            return customers;
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

}
