package fit5042.tutex.calculator;

import fit5042.tutex.repository.constants.CommonInstance;
import fit5042.tutex.repository.entities.Property;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.ejb.CreateException;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateful
public class ComparePropertySessionBean implements CompareProperty{
	
   private ArrayList<Property> propertyList = new ArrayList<>();
    
    
    @Override
    public void addProperty(Property property) {
        propertyList.add(property);
    }
    
    @Override
    public void removeProperty(Property property) {
    	propertyList.remove(property);
    }    

    @Override
    public int bestPerRoom() {
        Integer bestID=0;
        double bestPrice = 999999999;
        for(Property p : propertyList)
        {
            if(p.getPrice()/p.getNumberOfBedrooms()< bestPrice ) {
            	bestPrice = p.getPrice()/p.getNumberOfBedrooms();
            	bestID = p.getPropertyId();
            }
        }
        return bestID;
    }

    public CompareProperty create() throws CreateException, RemoteException {
        return null;
    }

    public void ejbCreate() throws CreateException {
    }
}
