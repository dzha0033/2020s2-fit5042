package fit5042.assignment.mbeans;

import fit5042.assignment.repository.CustomerRepository;
import fit5042.assignment.repository.entities.Address;
import fit5042.assignment.repository.entities.ContactPerson;
import fit5042.assignment.repository.entities.Customer;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
 *
 * 
 * @author Dongheng Zhan
 */
@ManagedBean(name = "customerManagedBean")
@SessionScoped

public class CustomerManagedBean implements Serializable {

    @EJB
    CustomerRepository customerRepository;
    
	private Set<ContactPerson> contactP;

    /**
     * Creates a new instance of CustomerManagedBean
     */
    
   

	public CustomerManagedBean() {
    }

    public List<Customer> getAllCustomers() {
        try {
            List<Customer> customers = customerRepository.getAllCustomers();
            return customers;
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void addCustomer(Customer customer) {
        try {
            customerRepository.addCustomer(customer);
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Search a Customer by Id
     */
    public Customer searchCustomerById(int id) {
        try {
            return customerRepository.searchCustomerById(id);
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }
   
    public List<ContactPerson> getAllContactPeople() throws Exception {
        try {
            return customerRepository.getAllContactPeople();
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    public void removeCustomer(int customerId) {
        try {
            customerRepository.removeCustomer(customerId);
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void editCustomer(Customer customer) {
        try {
            String c = customer.getAddress().getStreetNumber();
            Address address = customer.getAddress();
            address.setStreetNumber(c);
            customer.setAddress(address);

            customerRepository.editCustomer(customer);

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Customer has been updated succesfully"));
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    public void addCustomer(fit5042.assignment.controllers.Customer localCustomer) {
        //convert this newProperty which is the local property to entity property
        Customer customer = convertCustomerToEntity(localCustomer);

        try {
            customerRepository.addCustomer(customer);
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void addCustomer(fit5042.assignment.controllers.Customer localCustomer,int userId) {
        //convert this newProperty which is the local property to entity property
        Customer customer = convertCustomerToEntity(localCustomer,userId);

        try {
            customerRepository.addCustomer(customer);
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private Customer convertCustomerToEntity(fit5042.assignment.controllers.Customer localCustomer) {
        
        Customer customer = new Customer(); //entity
        String streetNumber = localCustomer.getStreetNumber();
        String streetAddress = localCustomer.getStreetAddress();
        String suburb = localCustomer.getSuburb();
        String postcode = localCustomer.getPostcode();
        String state = localCustomer.getState();
        Address address = new Address(streetNumber, streetAddress, suburb, postcode, state);
        customer.setUserId(localCustomer.getUserId());
        customer.setAddress(address);
        customer.setEmail(localCustomer.getEmail());
        customer.setName(localCustomer.getName());
        customer.setNumberOfOrders(localCustomer.getNumberOfOrders());
        customer.setCustomerId(localCustomer.getCustomerId());
        customer.setTypeOfIndustry(localCustomer.getTypeOfIndustry());
        customer.setUserType(localCustomer.getUserType());
        customer.setTags(localCustomer.getTags());
        
        List<ContactPerson> contacts = new ArrayList<ContactPerson>();
        
        try {
 	        for (ContactPerson contact : getAllContactPeople())
 	        { 
 	        	if (contact.getCustomer().getCustomerId() == localCustomer.getCustomerId()  )
 	            {   
 	        		contacts.add(contact); 
 	            }
 	        }
 	        }catch (Exception ex) {
 	            Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
 	        }
       
        contactP = new HashSet<>(contacts);
        
        customer.setContactPerson(contactP);
        /*for(ContactPerson contact)*/
        /*int conactPersonId = localCustomer.getConactPersonId();
        String name = localCustomer.getContactName();
        String phoneNumber = localCustomer.getPhoneNumber();
        ContactPerson contactPersons = new fit5042.assignment.repository.entities.ContactPerson(conactPersonId,name,phoneNumber,gender,email,address);
        if (contactPersons.getConactPersonId() == 0) {
            contactPersons = null;
        }
        customer.setContactPerson(contactPersons);*/
        return customer;
    }
    
 private Customer convertCustomerToEntity(fit5042.assignment.controllers.Customer localCustomer,int userId) {
        
        Customer customer = new Customer(); //entity
        String streetNumber = localCustomer.getStreetNumber();
        String streetAddress = localCustomer.getStreetAddress();
        String suburb = localCustomer.getSuburb();
        String postcode = localCustomer.getPostcode();
        String state = localCustomer.getState();
        Address address = new Address(streetNumber, streetAddress, suburb, postcode, state);
        customer.setUserId(userId);
        customer.setAddress(address);
        customer.setEmail(localCustomer.getEmail());
        customer.setName(localCustomer.getName());
        customer.setNumberOfOrders(localCustomer.getNumberOfOrders());
        customer.setCustomerId(localCustomer.getCustomerId());
        customer.setTypeOfIndustry(localCustomer.getTypeOfIndustry());
        customer.setUserType(localCustomer.getUserType());
        customer.setTags(localCustomer.getTags());
        
        List<ContactPerson> contacts = new ArrayList<ContactPerson>();
        
        try {
 	        for (ContactPerson contact : getAllContactPeople())
 	        { 
 	        	if (contact.getCustomer().getCustomerId() == localCustomer.getCustomerId()  )
 	            {   
 	        		contacts.add(contact); 
 	            }
 	        }
 	        }catch (Exception ex) {
 	            Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
 	        }
       
        contactP = new HashSet<>(contacts);
        
        customer.setContactPerson(contactP);
        /*for(ContactPerson contact)*/
        /*int conactPersonId = localCustomer.getConactPersonId();
        String name = localCustomer.getContactName();
        String phoneNumber = localCustomer.getPhoneNumber();
        ContactPerson contactPersons = new fit5042.assignment.repository.entities.ContactPerson(conactPersonId,name,phoneNumber,gender,email,address);
        if (contactPersons.getConactPersonId() == 0) {
            contactPersons = null;
        }
        customer.setContactPerson(contactPersons);*/
        return customer;
    }

}
