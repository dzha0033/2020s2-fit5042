package fit5042.assignment.mbeans;

import fit5042.assignment.repository.UserRepository;
import fit5042.assignment.repository.entities.Users;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.ExternalContextWrapper;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
*
* 
* @author Dongheng Zhan
*/
@ManagedBean(name = "usersManagedBean")
@SessionScoped
public class UsersManagedBean implements Serializable{
	
	@EJB
	UserRepository userRepository;
	

	public UsersManagedBean() {
    }

    public List<Users> getAllUsers() {
        try {
            List<Users> users = userRepository.getAllUsers();
            return users;
        } catch (Exception ex) {
            Logger.getLogger(UsersManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public void addUser(Users user) {
        try {
            userRepository.addUser(user);
        } catch (Exception ex) {
            Logger.getLogger(UsersManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public Users searchUsersById(int id) {
        try {
            return userRepository.searchUsersById(id);
        } catch (Exception ex) {
            Logger.getLogger(UsersManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }
    
    public void removeUser(int userId) {
        try {
        	userRepository.removeUser(userId);
        } catch (Exception ex) {
            Logger.getLogger(UsersManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void editUsers(Users user) {
        try {

            userRepository.editUsers(user);

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("User has been updated succesfully"));
        } catch (Exception ex) {
            Logger.getLogger(UsersManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void addUser(fit5042.assignment.controllers.Users localUser) {
        //convert this newProperty which is the local property to entity property
    	Users user = convertUsersToEntity(localUser);

        try {
            userRepository.addUser(user);
        } catch (Exception ex) {
            Logger.getLogger(UsersManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private Users convertUsersToEntity(fit5042.assignment.controllers.Users localUser) {
        
    	Users user = new Users();
        user.setUserId(localUser.getUserId());
        user.setUserName(localUser.getUserName());
        user.setPassword(localUser.getPassword());
        user.setTags(localUser.getTags());
        return user;
    }
    
    public String getUserName()
    {
    	
    	return (String) FacesContext.getCurrentInstance().getExternalContext().getRemoteUser();
    }
    public String name() {
    	String name = "staff";
    	return name;
    }

}

