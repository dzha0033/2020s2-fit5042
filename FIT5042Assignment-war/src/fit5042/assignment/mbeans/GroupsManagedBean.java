package fit5042.assignment.mbeans;

import fit5042.assignment.repository.GroupRepository;
import fit5042.assignment.repository.entities.Groups;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
*
* 
* @author Dongheng Zhan
*/
@ManagedBean(name = "groupsManagedBean")
@SessionScoped
public class GroupsManagedBean implements Serializable{
	
	@EJB
	GroupRepository groupRepository;
	

	public GroupsManagedBean() {
    }

    public List<Groups> getAllGroups() {
        try {
            List<Groups> groups = groupRepository.getAllGroups();
            return groups;
        } catch (Exception ex) {
            Logger.getLogger(GroupsManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public void addType(Groups groups) {
        try {
            groupRepository.addGroup(groups);
        } catch (Exception ex) {
            Logger.getLogger(GroupsManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public Groups searchGroupsById(int id) {
        try {
            return groupRepository.searchGroupsById(id);
        } catch (Exception ex) {
            Logger.getLogger(GroupsManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }
    
    public void removeGroups(int groupId) {
        try {
            groupRepository.removeGroups(groupId);
        } catch (Exception ex) {
            Logger.getLogger(GroupsManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void editGroups(Groups groups) {
        try {

            groupRepository.editGroups(groups);

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Groups has been updated succesfully"));
        } catch (Exception ex) {
            Logger.getLogger(GroupsManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void addGroup(fit5042.assignment.controllers.Groups localGroup) {
        //convert this newProperty which is the local property to entity property
    	Groups group = convertGroupsToEntity(localGroup);

        try {
            groupRepository.addGroup(group);
        } catch (Exception ex) {
            Logger.getLogger(GroupsManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private Groups convertGroupsToEntity(fit5042.assignment.controllers.Groups localGroup) {
        
    	Groups group = new Groups();
    	group.setGroupId(localGroup.getGroupId());
        group.setGroupName(localGroup.getGroupName());
        group.setUserName(localGroup.getUserName());
        group.setTags(localGroup.getTags());
        return group;
    }


}
