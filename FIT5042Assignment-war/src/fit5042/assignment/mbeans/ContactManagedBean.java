package fit5042.assignment.mbeans;

import fit5042.assignment.repository.ContactRepository;
import fit5042.assignment.repository.entities.Address;
import fit5042.assignment.repository.entities.ContactPerson;
import fit5042.assignment.repository.entities.Customer;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
*
* 
* @author Dongheng Zhan
*/
@ManagedBean(name = "contactManagedBean")
@SessionScoped

public class ContactManagedBean implements Serializable {

   @EJB
   ContactRepository contactRepository;

   public String name;
   
   private Customer customerNew;
   
   public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
   public ContactManagedBean() {
   }
   
   public List<Customer> getAllCustomers() {
       try {
           List<Customer> customers = contactRepository.getAllCustomers();
           return customers;
       } catch (Exception ex) {
           Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
       }
       return null;
   }

   public List<ContactPerson> getAllContacts() {
       try {
           List<ContactPerson> contacts = contactRepository.getAllContacts();
           return contacts;
       } catch (Exception ex) {
           Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
       }
       return null;
   }

   public void addContact(ContactPerson contact) {
       try {
           contactRepository.addContact(contact);
       } catch (Exception ex) {
           Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
       }
   }
   
   public Set<ContactPerson> searchContactByCustomerId(int customerId) {
       try {
           //retrieve contact person by id
           for (Customer customer : contactRepository.getAllCustomers()) {
               if (customer.getCustomerId() == customerId) {
                   return contactRepository.searchContactByCustomer(customer);
               }
           }
       } catch (Exception ex) {
           Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
       }

       return null;
   }

   /**
    * Search a Customer by Id
    */
   public ContactPerson searchContactById(int id) {
       try {
           return contactRepository.searchContactById(id);
       } catch (Exception ex) {
           Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
       }

       return null;
   }
  

   public void removeContact(int customerId) {
       try {
           contactRepository.removeContact(customerId);
       } catch (Exception ex) {
           Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
       }
   }

   public void editContact(ContactPerson contact) {
       try {
           String c = contact.getAddress().getStreetNumber();
           Address address = contact.getAddress();
           address.setStreetNumber(c);
           contact.setAddress(address);

           contactRepository.editContact(contact);

           FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Contact has been updated succesfully"));
       } catch (Exception ex) {
           Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
       }
   }

   public void addContact(fit5042.assignment.controllers.Contact localContact) {
       //convert this newProperty which is the local property to entity property
       ContactPerson contact = convertContactToEntity(localContact);

       try {
           contactRepository.addContact(contact);
       } catch (Exception ex) {
           Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
       }
   }
   
   public void addContact(fit5042.assignment.controllers.Contact localContact,int userId) {
       //convert this newProperty which is the local property to entity property
       ContactPerson contact = convertContactToEntity(localContact,userId);

       try {
           contactRepository.addContact(contact);
       } catch (Exception ex) {
           Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
       }
   }
	  
	  private ContactPerson convertContactToEntity(fit5042.assignment.controllers.Contact localContact) {
	        
	        ContactPerson contact = new ContactPerson(); //entity
	        String streetNumber = localContact.getStreetNumber();
	        String streetAddress = localContact.getStreetAddress();
	        String suburb = localContact.getSuburb();
	        String postcode = localContact.getPostcode();
	        String state = localContact.getState();
	        Address address = new Address(streetNumber, streetAddress, suburb, postcode, state);
	        String customerstreetNumber = localContact.getCustomerstreetNumber();
	        String customerstreetAddress = localContact.getCustomerstreetAddress();
	        String customersuburb = localContact.getCustomersuburb();
	        String customerpostcode = localContact.getCustomerpostcode();
	        String customerstate = localContact.getCustomerstate();
	        Address customeraddress = new Address(customerstreetNumber, customerstreetAddress, customersuburb, customerpostcode, customerstate);
	        contact.setAddress(address);
	        contact.setEmail(localContact.getEmail());
	        contact.setName(localContact.getName());
	        contact.setPhoneNumber(localContact.getPhoneNumber());
	        contact.setConactPersonId(localContact.getConactPersonId());
	        contact.setGender(localContact.getGender());
	        contact.setTags(localContact.getTags());
	        contact.setUserId(localContact.getUserId());
	        /*int customerId = localContact.getConactPersonId();
	        String customername = localContact.getCustomerName();
	        String customerEmail = localContact.getCustomerEmail();
	        String typeOfIndustry = localContact.getTypeOfIndustry();
	        int numberOfOrders = localContact.getNumberOfOrders();
	        String userType = localContact.getUserType();
	        Set<String> customertags = localContact.getCustomertags();*/
	        String[] nameArray = {}; 
	        nameArray = name.split("\\-");
	        int idd = Integer.parseInt(nameArray[0].trim());
	         customerNew  = new Customer();
	        try {
	        for (Customer customer : contactRepository.getAllCustomers())
	        { 
	        	if (customer.getCustomerId() == idd )
	            {   
	        	   customerNew = customer; 
	            }
	        }
	        }catch (Exception ex) {
	            Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
	        }
	       /* Customer customer = new fit5042.assignment.repository.entities.Customer( Integer.parseInt(nameArray[0]),customeraddress, numberOfOrders ,customername, typeOfIndustry, customerEmail,
	    			userType,customertags);*/
	        
	        if (customerNew.getCustomerId() == 0) {
	        	customerNew = null;
	        }
	        contact.setCustomer(customerNew);
	        return contact;
	    }
	  
	  private ContactPerson convertContactToEntity(fit5042.assignment.controllers.Contact localContact, int userId) {
	        
	        ContactPerson contact = new ContactPerson(); //entity
	        String streetNumber = localContact.getStreetNumber();
	        String streetAddress = localContact.getStreetAddress();
	        String suburb = localContact.getSuburb();
	        String postcode = localContact.getPostcode();
	        String state = localContact.getState();
	        Address address = new Address(streetNumber, streetAddress, suburb, postcode, state);
	        String customerstreetNumber = localContact.getCustomerstreetNumber();
	        String customerstreetAddress = localContact.getCustomerstreetAddress();
	        String customersuburb = localContact.getCustomersuburb();
	        String customerpostcode = localContact.getCustomerpostcode();
	        String customerstate = localContact.getCustomerstate();
	        Address customeraddress = new Address(customerstreetNumber, customerstreetAddress, customersuburb, customerpostcode, customerstate);
	        contact.setAddress(address);
	        contact.setEmail(localContact.getEmail());
	        contact.setName(localContact.getName());
	        contact.setPhoneNumber(localContact.getPhoneNumber());
	        contact.setConactPersonId(localContact.getConactPersonId());
	        contact.setGender(localContact.getGender());
	        contact.setTags(localContact.getTags());
	        contact.setUserId(userId);
	        /*int customerId = localContact.getConactPersonId();
	        String customername = localContact.getCustomerName();
	        String customerEmail = localContact.getCustomerEmail();
	        String typeOfIndustry = localContact.getTypeOfIndustry();
	        int numberOfOrders = localContact.getNumberOfOrders();
	        String userType = localContact.getUserType();
	        Set<String> customertags = localContact.getCustomertags();*/
	        String[] nameArray = {}; 
	        nameArray = name.split("\\-");
	        int idd = Integer.parseInt(nameArray[0].trim());
	         customerNew  = new Customer();
	        try {
	        for (Customer customer : contactRepository.getAllCustomers())
	        { 
	        	if (customer.getCustomerId() == idd )
	            {   
	        	   customerNew = customer; 
	            }
	        }
	        }catch (Exception ex) {
	            Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
	        }
	       /* Customer customer = new fit5042.assignment.repository.entities.Customer( Integer.parseInt(nameArray[0]),customeraddress, numberOfOrders ,customername, typeOfIndustry, customerEmail,
	    			userType,customertags);*/
	        
	        if (customerNew.getCustomerId() == 0) {
	        	customerNew = null;
	        }
	        contact.setCustomer(customerNew);
	        return contact;
	    }
}
