package fit5042.assignment.mbeans;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import fit5042.assignment.controllers.SessionUtils;

@ManagedBean
@SessionScoped
public class LogoutManagedBean implements Serializable {
	
	public String logout() {
		HttpSession session = SessionUtils.getSession();
		session.invalidate();
		return "http://localhost:8080/FIT5042Assignment-war/";
	}
}
