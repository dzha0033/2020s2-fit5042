package fit5042.assignment.mbeans;

import fit5042.assignment.repository.TypeOfIndustryRepository;
import fit5042.assignment.repository.entities.TypeOfIndustry;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
*
* 
* @author Dongheng Zhan
*/
@ManagedBean(name = "typeManagedBean")
@SessionScoped
public class TypeManagedBean implements Serializable{
	
	@EJB
	TypeOfIndustryRepository typeRepository;
	

	public TypeManagedBean() {
    }

    public List<TypeOfIndustry> getAllTypes() {
        try {
            List<TypeOfIndustry> types = typeRepository.getAllTypes();
            return types;
        } catch (Exception ex) {
            Logger.getLogger(TypeManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public void addType(TypeOfIndustry type) {
        try {
            typeRepository.addType(type);
        } catch (Exception ex) {
            Logger.getLogger(TypeManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public TypeOfIndustry searchTypeById(int id) {
        try {
            return typeRepository.searchTypeById(id);
        } catch (Exception ex) {
            Logger.getLogger(TypeManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }
    
    public void removeType(int typeId) {
        try {
            typeRepository.removeType(typeId);
        } catch (Exception ex) {
            Logger.getLogger(TypeManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void editType(TypeOfIndustry type) {
        try {

            typeRepository.editType(type);

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Type has been updated succesfully"));
        } catch (Exception ex) {
            Logger.getLogger(TypeManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void addType(fit5042.assignment.controllers.Type localType) {
        //convert this newProperty which is the local property to entity property
        TypeOfIndustry type = convertTypeToEntity(localType);

        try {
            typeRepository.addType(type);
        } catch (Exception ex) {
            Logger.getLogger(TypeManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private TypeOfIndustry convertTypeToEntity(fit5042.assignment.controllers.Type localType) {
        
        TypeOfIndustry type = new TypeOfIndustry();
        type.setTypeId(localType.getTypeId());
        type.setType(localType.getTypeOfIndustry());
        type.setTags(localType.getTags());
        return type;
    }


}
