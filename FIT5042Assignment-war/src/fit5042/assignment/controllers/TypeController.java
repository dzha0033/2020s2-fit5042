package fit5042.assignment.controllers;


import javax.el.ELContext;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;

/**
 *
 * 
 * @author Dongheng Zhan
 */
@Named(value = "typeController")
@RequestScoped
public class TypeController {
	private int typeId;

	public int getTypeId() {
		return typeId;
	}

	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}

	private fit5042.assignment.repository.entities.TypeOfIndustry type;
	
	 public TypeController() {
	    
		 typeId = Integer.valueOf(FacesContext.getCurrentInstance()
	                .getExternalContext()
	                .getRequestParameterMap()
	                .get("typeID"));
	       
		 type = getType();
	    }
	 
	 public fit5042.assignment.repository.entities.TypeOfIndustry getType() {
	        if (type == null) {
	             
	            ELContext context
	                    = FacesContext.getCurrentInstance().getELContext();
	            TypeApplication app
	                    = (TypeApplication) FacesContext.getCurrentInstance()
	                            .getApplication()
	                            .getELResolver()
	                            .getValue(context, null, "typeApplication");
	           
	            return app.getTypes().get(--typeId);
	        }
	        return type;
	    }
     
}
