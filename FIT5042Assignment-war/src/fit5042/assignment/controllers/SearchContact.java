package fit5042.assignment.controllers;

import javax.el.ELContext;
import fit5042.assignment.repository.entities.*;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import fit5042.assignment.mbeans.ContactManagedBean;
import javax.faces.bean.ManagedProperty;

/**
 *
 * @author Dongheng Zhan
 */
@RequestScoped
@Named("searchContact")
public class SearchContact {

	private boolean showForm = true;

	private Contact contact;

	ContactApplication app;
	
	private int customerId;

	private int searchByInt;

	private int userId;
	
	
	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public ContactApplication getApp() {
		return app;
	}

	public void setApp(ContactApplication app) {
		this.app = app;
	}

	
	
	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public int getSearchByInt() {
		return searchByInt;
	}

	public void setSearchByInt(int searchByInt) {
		this.searchByInt = searchByInt;
	}

	public void setContactPerson(Contact contact) {
		this.contact = contact;
	}

	public Contact getContactPerson() {
		return contact;
	}

	public boolean isShowForm() {
		return showForm;
	}

	public SearchContact() {
		ELContext context = FacesContext.getCurrentInstance().getELContext();

		app = (ContactApplication) FacesContext.getCurrentInstance().getApplication().getELResolver().getValue(context,
				null, "contactApplication");

		app.updateContactList();

	}

	public void searchContactById(int contactId) {
		try {
			// search this Customer then refresh the list in CustomerApplication bean
			app.searchContactById(contactId);
		} catch (Exception ex) {

		}
		showForm = true;

	}
	
	 public void searchContactByCustomerId(int customerId) {
	        try {
	        	int p = customerId;
	            //search all properties by contact person from db via EJB 
	            app.searchContactByCustomerId(customerId);
	        } catch (Exception ex) {

	        }
	        showForm = true;
	    }
	 
	 public void searchPart(int userId) {
			try {
				// return all properties from db via EJB
				app.searchPart(userId);
			} catch (Exception ex) {

			}
			showForm = true;
		}

	public void searchAll() {
		try {
			// return all properties from db via EJB
			app.searchAll();
		} catch (Exception ex) {

		}
		showForm = true;
	}

}
