package fit5042.assignment.controllers;

import javax.el.ELContext;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;

/**
 *
 * 
 * @author Dongheng Zhan
 */
@Named(value = "contactController")
@RequestScoped
public class ContactController {
	
	private int contactId;

	public int getContactId() {
		return contactId;
	}

	public void setContactId(int contactId) {
		this.contactId = contactId;
	}

	private fit5042.assignment.repository.entities.ContactPerson contactPerson;
	
	 public ContactController() {
	    
	    	contactId = Integer.valueOf(FacesContext.getCurrentInstance()
	                .getExternalContext()
	                .getRequestParameterMap()
	                .get("contactID"));
	       
	    	contactPerson = getContact();
	    }
	 
	 public fit5042.assignment.repository.entities.ContactPerson getContact() {
	        if (contactPerson == null) {
	             
	            ELContext context
	                    = FacesContext.getCurrentInstance().getELContext();
	            ContactApplication app
	                    = (ContactApplication) FacesContext.getCurrentInstance()
	                            .getApplication()
	                            .getELResolver()
	                            .getValue(context, null, "contactApplication");
	           
	            return app.getCustomerByID(contactId);
	        }
	        return contactPerson;
	    }
     
}
