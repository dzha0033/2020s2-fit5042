package fit5042.assignment.controllers;


import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.assignment.mbeans.TypeManagedBean;
import fit5042.assignment.repository.entities.*;
import javax.faces.bean.ManagedProperty;

/**
*
* @author Dongheng Zhan
*/
@RequestScoped
@Named("removeType")
public class RemoveType {
	
	@ManagedProperty(value = "#{typeManagedBean}")
	TypeManagedBean typeManagedBean;

	private boolean showForm = true;

	private Type type;

	TypeApplication app;

	public void setType(Type type) {
		this.type = type;
	}

	public Type getGroup() {
		return type;
	}

	public boolean isShowForm() {
		return showForm;
	}

	public RemoveType() {
		ELContext context = FacesContext.getCurrentInstance().getELContext();

		app = (TypeApplication) FacesContext.getCurrentInstance().getApplication().getELResolver().getValue(context,
				null, "typeApplication");

		ELContext elContext = FacesContext.getCurrentInstance().getELContext();
		typeManagedBean = (TypeManagedBean) FacesContext.getCurrentInstance().getApplication().getELResolver()
				.getValue(elContext, null, "typeManagedBean");
	}

	public void removeType(int typeId) {
		try {
			// remove this Customer from db via EJB
			typeManagedBean.removeType(typeId);

			// refresh the list in CustomerApplication bean
			app.searchAll();

			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage("Type has been deleted succesfully"));
		} catch (Exception ex) {

		}
		showForm = true;

	}

}
