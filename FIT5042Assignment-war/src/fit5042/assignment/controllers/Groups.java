package fit5042.assignment.controllers;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@RequestScoped
@Named(value = "groups")
public class Groups implements Serializable{
	
	private int groupId;
	private String userName;
	private String groupName;
	private Set<String> tags;
	
	
	public Groups() {
		this.tags = new HashSet<>();
	}
	
	public Groups(int groupId,String userName, String groupName,Set<String> tags) {
		this.groupId = groupId;
		this.userName  = userName;
		this.groupName = groupName;
		this.tags = tags;	
	}

	public int getGroupId() {
		return groupId;
	}

	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Set<String> getTags() {
		return tags;
	}

	public void setTags(Set<String> tags) {
		this.tags = tags;
	}
	

}
