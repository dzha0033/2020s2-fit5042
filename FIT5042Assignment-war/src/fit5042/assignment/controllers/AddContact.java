package fit5042.assignment.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import fit5042.assignment.mbeans.ContactManagedBean;

import javax.faces.bean.ManagedProperty;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Dongheng Zhan
 */

@RequestScoped
@Named("addContact")
public class AddContact {
	
	 @ManagedProperty(value = "#{contactManagedBean}")
	    ContactManagedBean contactManagedBean;

	    private boolean showForm = true;

	    private Contact contact;

	    ContactApplication app;
	    
	    public void setContact(Contact contact) {
	        this.contact = contact;
	    }

	    public Contact getContact() {
	        return contact;
	    }

	    public boolean isShowForm() {
	        return showForm;
	    }
	    
	    public AddContact() {
	        ELContext context
	                = FacesContext.getCurrentInstance().getELContext();

	        app = (ContactApplication) FacesContext.getCurrentInstance()
	                .getApplication()
	                .getELResolver()
	                .getValue(context, null, "contactApplication");

	        //instantiate CustomerManagedBean
	        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
	        contactManagedBean = (ContactManagedBean) FacesContext.getCurrentInstance().getApplication()
	                .getELResolver().getValue(elContext, null, "contactManagedBean");
	    }
	    
	    public void addContact (Contact localContact,int userId) {
	        //this is the local Customer, not the entity
	        try {
	            //add this Customer to db via EJB
	             contactManagedBean.addContact(localContact,userId);;

	            //refresh the list in CustomerApplication bean
	             app.searchAll();

	            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Contact person has been added succesfully"));
	        } catch (Exception ex) {

	        }
	        showForm = true;
	    }
	    
	    public void addContact (Contact localContact) {
	        //this is the local Customer, not the entity
	        try {
	            //add this Customer to db via EJB
	             contactManagedBean.addContact(localContact);;

	            //refresh the list in CustomerApplication bean
	             app.searchAll();

	            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Contact person has been added succesfully"));
	        } catch (Exception ex) {

	        }
	        showForm = true;
	    }
}
