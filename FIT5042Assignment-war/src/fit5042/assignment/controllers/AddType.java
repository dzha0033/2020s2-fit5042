package fit5042.assignment.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import fit5042.assignment.mbeans.TypeManagedBean;

import javax.faces.bean.ManagedProperty;

/**
*
* @author Dongheng Zhan
*/
@RequestScoped
@Named("addType")
public class AddType {
	@ManagedProperty(value = "#{typeManagedBean}")
    TypeManagedBean typeManagedBean;

    private boolean showForm = true;

    private Type type;

    TypeApplication app;

    public void setType(Type type) {
        this.type = type;
    }

    public Type getType() {
        return type;
    }

    public boolean isShowForm() {
        return showForm;
    }

    public AddType() {
        ELContext context
                = FacesContext.getCurrentInstance().getELContext();

        app = (TypeApplication) FacesContext.getCurrentInstance()
                .getApplication()
                .getELResolver()
                .getValue(context, null, "typeApplication");

        //instantiate CustomerManagedBean
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        typeManagedBean = (TypeManagedBean) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(elContext, null, "typeManagedBean");
    }

    public void addType(Type localType) {
        //this is the local Customer, not the entity
        try {
            //add this Customer to db via EJB
             typeManagedBean.addType(localType);;

            //refresh the list in CustomerApplication bean
             app.searchAll();

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Type has been added succesfully"));
        } catch (Exception ex) {

        }
        showForm = true;
    }

}
