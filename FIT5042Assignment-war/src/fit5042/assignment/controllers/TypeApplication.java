package fit5042.assignment.controllers;

import java.util.ArrayList;
import java.util.Set;

import javax.enterprise.context.ApplicationScoped;

import fit5042.assignment.mbeans.TypeManagedBean;

import javax.inject.Named;
import fit5042.assignment.repository.entities.TypeOfIndustry;

import javax.el.ELContext;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;

/**
 * The class is a demonstration of how the application scope works. You can
 * change this scope.
 *
 * @author Dongheng Zhan
 */
@Named(value = "typeApplication")
@ApplicationScoped

public class TypeApplication {

	@ManagedProperty(value = "#{typeManagedBean}")
    TypeManagedBean typeManagedBean;

    private ArrayList<TypeOfIndustry> types;

    private boolean showForm = true;

    public boolean isShowForm() {
        return showForm;
    }
    
    public TypeApplication() throws Exception {
        types = new ArrayList<>();

        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        typeManagedBean = (TypeManagedBean) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(elContext, null, "typeManagedBean");

        //get properties from db 
        updateTypeList();
    }
    public ArrayList<TypeOfIndustry> getTypes() {
        return types;
    }

    private void setTypes(ArrayList<TypeOfIndustry> newTypes) {
    	
        this.types = newTypes;
    }

    
    public void updateTypeList() {
        if (types != null && types.size() > 0)
        {
            
        }
        else
        {
        	types.clear();

            for (fit5042.assignment.repository.entities.TypeOfIndustry type : typeManagedBean.getAllTypes())
            {
            	types.add(type);
            }

            setTypes(types);
        }
    }
    
    public void searchTypeById(int typeId) {
        types.clear();

        types.add(typeManagedBean.searchTypeById(typeId));
    }
    
    public void searchAll()
    {
    	types.clear();
        
        for (fit5042.assignment.repository.entities.TypeOfIndustry type : typeManagedBean.getAllTypes())
        {
        	types.add(type);
        }
        
        setTypes(types);
    }

}
