package fit5042.assignment.controllers;

import javax.el.ELContext;
import fit5042.assignment.repository.entities.*;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import fit5042.assignment.mbeans.TypeManagedBean;
import javax.faces.bean.ManagedProperty;

/**
 *
 * @author Dongheng Zhan
 */
@RequestScoped
@Named("searchType")
public class SearchType {
	
	private boolean showForm = true;

	private Type type;

	TypeApplication app;
	
	private int typeId;

	private int searchByInt;

	
	
	public TypeApplication getApp() {
		return app;
	}

	public void setApp(TypeApplication app) {
		this.app = app;
	}

	
	
	public int getTypeId() {
		return typeId;
	}

	public void setCustomerId(int typeId) {
		this.typeId = typeId;
	}

	public int getSearchByInt() {
		return searchByInt;
	}

	public void setSearchByInt(int searchByInt) {
		this.searchByInt = searchByInt;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public Type getType() {
		return type;
	}

	public boolean isShowForm() {
		return showForm;
	}

	public SearchType() {
		ELContext context = FacesContext.getCurrentInstance().getELContext();

		app = (TypeApplication) FacesContext.getCurrentInstance().getApplication().getELResolver().getValue(context,
				null, "typeApplication");

		app.updateTypeList();

	}

	public void searchTypeById(int typeId) {
		try {
			// search this Customer then refresh the list in CustomerApplication bean
			app.searchTypeById(typeId);
		} catch (Exception ex) {

		}
		showForm = true;

	}
	

	public void searchAll() {
		try {
			// return all properties from db via EJB
			app.searchAll();
		} catch (Exception ex) {

		}
		showForm = true;
	}

}
