package fit5042.assignment.controllers;

import fit5042.assignment.repository.entities.Address;
import fit5042.assignment.repository.entities.Customer;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
*
* @author Dongheng Zhan
*/
@RequestScoped
@Named(value = "contact")
public class Contact implements Serializable {
	
	
	private int conactPersonId;
	private String name;
	private String gender;
	private String email;
	private String phoneNumber;
	private int userId;
	
	private Address address;
	private Address customeraddress;
	private Customer customer;
	
	private String streetNumber;
    private String streetAddress;
    private String suburb;
    private String postcode;
    private String state;
	
    private int customerId;
	private String customerName;
	private String typeOfIndustry;
	private String customerEmail;
	private int numberOfOrders;
	private String userType;
	
	private String customerstreetNumber;
    private String customerstreetAddress;
    private String customersuburb;
    private String customerpostcode;
    private String customerstate;
    
	private Set<String> tags;
	private Set<String> customertags;
	private Set<fit5042.assignment.repository.entities.ContactPerson> contacts;
	
	public Contact() {
        this.tags = new HashSet<>();
    }
	
	public Contact(int customerId, Address address, String phoneNumber, String name, String gender,String email,
			Customer customer,String userType, int userId,Set<String> tags) {
		this.userId = userId;
		this.customerId = customerId;
		this.address = address;
		this.name = name;
		this.gender = gender;
		this.email = email;
		this.userType = userType;
		this.phoneNumber = phoneNumber;
		this.customer = customer;
		this.tags = tags;
	}

	public int getConactPersonId() {
		return conactPersonId;
	}

	public void setConactPersonId(int conactPersonId) {
		this.conactPersonId = conactPersonId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public String getStreetNumber() {
		return streetNumber;
	}

	public void setStreetNumber(String streetNumber) {
		this.streetNumber = streetNumber;
	}

	public String getStreetAddress() {
		return streetAddress;
	}

	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	public String getSuburb() {
		return suburb;
	}

	public void setSuburb(String suburb) {
		this.suburb = suburb;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getTypeOfIndustry() {
		return typeOfIndustry;
	}

	public void setTypeOfIndustry(String typeOfIndustry) {
		this.typeOfIndustry = typeOfIndustry;
	}

	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	public int getNumberOfOrders() {
		return numberOfOrders;
	}

	public void setNumberOfOrders(int numberOfOrders) {
		this.numberOfOrders = numberOfOrders;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public Set<String> getTags() {
		return tags;
	}

	public void setTags(Set<String> tags) {
		this.tags = tags;
	}

	public Set<fit5042.assignment.repository.entities.ContactPerson> getContacts() {
		return contacts;
	}

	public void setContacts(Set<fit5042.assignment.repository.entities.ContactPerson> contacts) {
		this.contacts = contacts;
	}

	public Address getCustomeraddress() {
		return customeraddress;
	}

	public void setCustomeraddress(Address customeraddress) {
		this.customeraddress = customeraddress;
	}

	public String getCustomerstreetNumber() {
		return customerstreetNumber;
	}

	public void setCustomerstreetNumber(String customerstreetNumber) {
		this.customerstreetNumber = customerstreetNumber;
	}

	public String getCustomerstreetAddress() {
		return customerstreetAddress;
	}

	public void setCustomerstreetAddress(String customerstreetAddress) {
		this.customerstreetAddress = customerstreetAddress;
	}

	public String getCustomersuburb() {
		return customersuburb;
	}

	public void setCustomersuburb(String customersuburb) {
		this.customersuburb = customersuburb;
	}

	public String getCustomerpostcode() {
		return customerpostcode;
	}

	public void setCustomerpostcode(String customerpostcode) {
		this.customerpostcode = customerpostcode;
	}

	public String getCustomerstate() {
		return customerstate;
	}

	public void setCustomerstate(String customerstate) {
		this.customerstate = customerstate;
	}

	public Set<String> getCustomertags() {
		return customertags;
	}

	public void setCustomertags(Set<String> customertags) {
		this.customertags = customertags;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "Contact [conactPersonId=" + conactPersonId + ", name=" + name + ", gender=" + gender + ", email="
				+ email + ", phoneNumber=" + phoneNumber + ", address=" + address + ", customeraddress="
				+ customeraddress + ", customer=" + customer + ", streetNumber=" + streetNumber + ", streetAddress="
				+ streetAddress + ", suburb=" + suburb + ", postcode=" + postcode + ", state=" + state + ", customerId="
				+ customerId + ", customerName=" + customerName + ", typeOfIndustry=" + typeOfIndustry
				+ ", customerEmail=" + customerEmail + ", numberOfOrders=" + numberOfOrders + ", userType=" + userType
				+ ", customerstreetNumber=" + customerstreetNumber + ", customerstreetAddress=" + customerstreetAddress
				+ ", customersuburb=" + customersuburb + ", customerpostcode=" + customerpostcode + ", customerstate="
				+ customerstate + ", tags=" + tags + ", customertags=" + customertags + ", contacts=" + contacts + "]";
	}
	
	
}
