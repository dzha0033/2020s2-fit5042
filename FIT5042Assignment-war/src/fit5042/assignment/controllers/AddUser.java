package fit5042.assignment.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import fit5042.assignment.mbeans.UsersManagedBean;

import javax.faces.bean.ManagedProperty;

/**
*
* @author Dongheng Zhan
*/
@RequestScoped
@Named("addUser")
public class AddUser {
	@ManagedProperty(value = "#{userManagedBean}")
    UsersManagedBean usersManagedBean;

    private boolean showForm = true;

    private Users user;

    UserApplication app;

    public void setUser(Users user) {
        this.user = user;
    }

    public Users getUser() {
        return user;
    }

    public boolean isShowForm() {
        return showForm;
    }

    public AddUser() {
        ELContext context
                = FacesContext.getCurrentInstance().getELContext();

        app = (UserApplication) FacesContext.getCurrentInstance()
                .getApplication()
                .getELResolver()
                .getValue(context, null, "userApplication");

        //instantiate CustomerManagedBean
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        usersManagedBean = (UsersManagedBean) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(elContext, null, "usersManagedBean");
    }

    public void addUser(Users localUser) {
        //this is the local Customer, not the entity
        try {
            //add this Customer to db via EJB
             usersManagedBean.addUser(localUser);;

            //refresh the list in CustomerApplication bean
             app.searchAll();

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("User has been added succesfully"));
        } catch (Exception ex) {

        }
        showForm = true;
    }


}
