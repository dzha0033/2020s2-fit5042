package fit5042.assignment.controllers;

import fit5042.assignment.repository.entities.Customer;
import fit5042.assignment.mbeans.ContactManagedBean;
import java.util.ArrayList;
import java.util.List;
import javax.el.ELContext;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

@FacesConverter(forClass = fit5042.assignment.repository.entities.Customer.class, value = "customer")

/**
*
* @author Dongheng Zhan
*/
public class CustomerConverter implements Converter {

    @ManagedProperty(value = "#{contactManagedBean}")
    ContactManagedBean contactManagedBean;

    public List<Customer> customerDB; //= propertyManagedBean.getAllContactPeople();

    public CustomerConverter() {
        try {
            //instantiate propertyManagedBean
            ELContext elContext = FacesContext.getCurrentInstance().getELContext();
            contactManagedBean = (ContactManagedBean) FacesContext.getCurrentInstance().getApplication()
                    .getELResolver().getValue(elContext, null, "contactManagedBean");

            customerDB = contactManagedBean.getAllCustomers();
        } catch (Exception ex) {

        }
    }

    //this method is for converting the submitted value (as String) to the contact person object
    //the reason for using this method is, the dropdown box in the xhtml does not capture the contact person object, but the String.
    public Customer getAsObject(FacesContext facesContext, UIComponent component, String submittedValue) {
        if (submittedValue.trim().equals("")) {
            return null;
        } else {
            try {
                int number = Integer.parseInt(submittedValue);

                for (Customer c : customerDB) {
                    if (c.getCustomerId() == number) {
                        return c;
                    }
                }

            } catch (NumberFormatException exception) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid Customer"));
            }
        }

        return null;
    }

    public String getAsString(FacesContext facesContext, UIComponent component, Object value) {
        if (value == null || value.equals("")) {
            return "";
        } else {
            return String.valueOf(((Customer) value).getCustomerId());
        }
    }
}
