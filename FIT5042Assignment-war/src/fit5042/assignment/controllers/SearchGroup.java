package fit5042.assignment.controllers;

import javax.el.ELContext;
import fit5042.assignment.repository.entities.*;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import fit5042.assignment.mbeans.GroupsManagedBean;
import javax.faces.bean.ManagedProperty;

/**
 *
 * @author Dongheng Zhan
 */
@RequestScoped
@Named("searchGroup")
public class SearchGroup {
	
	private boolean showForm = true;

	private Groups group;

	GroupApplication app;
	
	private int groupId;

	private int searchByInt;

	
	
	public GroupApplication getApp() {
		return app;
	}

	public void setApp(GroupApplication app) {
		this.app = app;
	}

	
	
	public int getGroupId() {
		return groupId;
	}

	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}

	public int getSearchByInt() {
		return searchByInt;
	}

	public void setSearchByInt(int searchByInt) {
		this.searchByInt = searchByInt;
	}

	public void setGroup(Groups group) {
		this.group = group;
	}

	public Groups getGroup() {
		return group;
	}

	public boolean isShowForm() {
		return showForm;
	}

	public SearchGroup() {
		ELContext context = FacesContext.getCurrentInstance().getELContext();

		app = (GroupApplication) FacesContext.getCurrentInstance().getApplication().getELResolver().getValue(context,
				null, "groupApplication");

		app.updateGroupList();

	}

	public void searchGroupsById(int userId) {
		try {
			// search this Customer then refresh the list in CustomerApplication bean
			app.searchGroupsById(userId);
		} catch (Exception ex) {

		}
		showForm = true;

	}
	

	public void searchAll() {
		try {
			// return all properties from db via EJB
			app.searchAll();
		} catch (Exception ex) {

		}
		showForm = true;
	}

}
