package fit5042.assignment.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import fit5042.assignment.mbeans.ContactManagedBean;
import fit5042.assignment.repository.entities.*;
import javax.faces.bean.ManagedProperty;

/**
 *
 * @author Dongheng Zhan
 */
@RequestScoped
@Named("removeContact")
public class RemoveContact {

	@ManagedProperty(value = "#{contactManagedBean}")
	ContactManagedBean contactManagedBean;

	private boolean showForm = true;

	private Contact contact;

	ContactApplication app;

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public Contact getContact() {
		return contact;
	}

	public boolean isShowForm() {
		return showForm;
	}

	public RemoveContact() {
		ELContext context = FacesContext.getCurrentInstance().getELContext();

		app = (ContactApplication) FacesContext.getCurrentInstance().getApplication().getELResolver().getValue(context,
				null, "contactApplication");

		ELContext elContext = FacesContext.getCurrentInstance().getELContext();
		contactManagedBean = (ContactManagedBean) FacesContext.getCurrentInstance().getApplication().getELResolver()
				.getValue(elContext, null, "contactManagedBean");
	}

	public void removeContact(int contactId) {
		try {
			// remove this Customer from db via EJB
			contactManagedBean.removeContact(contactId);

			// refresh the list in CustomerApplication bean
			app.searchAll();

			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage("Contact person has been deleted succesfully"));
		} catch (Exception ex) {

		}
		showForm = true;

	}
}
