package fit5042.assignment.controllers;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@RequestScoped
@Named(value = "users")
public class Users implements Serializable{
	
	private int userId;
	private String userName;
	private String password;
	private Set<String> tags;
	
	public Users() {
		this.tags = new HashSet<>();
	}
	
	public Users(int userId,String userName, String password,Set<String> tags) {
		this.userId = userId;
		this.userName  = userName;
		this.password = password;
		this.tags = tags;	
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Set<String> getTags() {
		return tags;
	}

	public void setTags(Set<String> tags) {
		this.tags = tags;
	}

	@Override
	public String toString() {
		return userName ;
	}

	

}
