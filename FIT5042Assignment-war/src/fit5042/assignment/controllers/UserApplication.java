package fit5042.assignment.controllers;

import java.util.ArrayList;
import java.util.Set;

import javax.enterprise.context.ApplicationScoped;

import fit5042.assignment.mbeans.UsersManagedBean;

import javax.inject.Named;

import fit5042.assignment.repository.entities.Users;

import javax.el.ELContext;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;

/**
 * The class is a demonstration of how the application scope works. You can
 * change this scope.
 *
 * @author Dongheng Zhan
 */
@Named(value = "userApplication")
@ApplicationScoped


public class UserApplication {
	
	@ManagedProperty(value = "#{usersManagedBean}")
    UsersManagedBean usersManagedBean;

    private ArrayList<Users> users;

    private boolean showForm = true;

    public boolean isShowForm() {
        return showForm;
    }
    
    public UserApplication() throws Exception {
    	users = new ArrayList<>();

        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        usersManagedBean = (UsersManagedBean) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(elContext, null, "usersManagedBean");

        //get properties from db 
        updateUserList();
    }
    
    public Users getUserByID(int userID) {
    	Users user = new Users();
    	for(Users us:getUsers()) {
    		if (us.getUserId() == userID) {
    			return us;
    		}
    	}
		return user;
    }
    
    public Users getUserByName(String username) {
    	Users user = new Users();
    	for(Users us:getUsers()) {
    		if (us.getUserName().equals(username)) {
    			return us;
    		}
    	}
		return user;
    }
    public ArrayList<Users> getUsers() {
        return users;
    }

    private void setUsers(ArrayList<Users> newUsers) {
    	
        this.users = newUsers;
    }

    
    public void updateUserList() {
        if (users != null && users.size() > 0)
        {
            
        }
        else
        {
        	users.clear();

            for (fit5042.assignment.repository.entities.Users user : usersManagedBean.getAllUsers())
            {
            	users.add(user);
            }

            setUsers(users);
        }
    }
    
    public void searchUsersById(int userId) {
        users.clear();

        users.add(usersManagedBean.searchUsersById(userId));
    }
    
    public void searchAll()
    {
    	users.clear();
        
        for (fit5042.assignment.repository.entities.Users user : usersManagedBean.getAllUsers())
        {
        	users.add(user);
        }
        
        setUsers(users);
    }
}
