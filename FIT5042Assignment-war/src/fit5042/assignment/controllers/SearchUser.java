package fit5042.assignment.controllers;

import javax.el.ELContext;
import fit5042.assignment.repository.entities.*;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import fit5042.assignment.mbeans.UsersManagedBean;
import javax.faces.bean.ManagedProperty;

/**
 *
 * @author Dongheng Zhan
 */
@RequestScoped
@Named("searchUser")
public class SearchUser {
	
	private boolean showForm = true;

	private Users user;

	UserApplication app;
	
	private int userId;

	private int searchByInt;

	
	
	public UserApplication getApp() {
		return app;
	}

	public void setApp(UserApplication app) {
		this.app = app;
	}

	
	
	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getSearchByInt() {
		return searchByInt;
	}

	public void setSearchByInt(int searchByInt) {
		this.searchByInt = searchByInt;
	}

	public void setUser(Users user) {
		this.user = user;
	}

	public Users getUser() {
		return user;
	}

	public boolean isShowForm() {
		return showForm;
	}

	public SearchUser() {
		ELContext context = FacesContext.getCurrentInstance().getELContext();

		app = (UserApplication) FacesContext.getCurrentInstance().getApplication().getELResolver().getValue(context,
				null, "userApplication");

		app.updateUserList();

	}

	public void searchUserById(int userId) {
		try {
			// search this Customer then refresh the list in CustomerApplication bean
			app.searchUsersById(userId);
		} catch (Exception ex) {

		}
		showForm = true;

	}
	

	public void searchAll() {
		try {
			// return all properties from db via EJB
			app.searchAll();
		} catch (Exception ex) {

		}
		showForm = true;
	}

}
