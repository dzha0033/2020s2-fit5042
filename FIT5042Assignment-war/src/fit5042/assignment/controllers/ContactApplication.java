package fit5042.assignment.controllers;

import java.util.ArrayList;
import java.util.Set;

import javax.enterprise.context.ApplicationScoped;
import fit5042.assignment.mbeans.ContactManagedBean;

import javax.inject.Named;
import fit5042.assignment.repository.entities.ContactPerson;

import javax.el.ELContext;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;

/**
 * The class is a demonstration of how the application scope works. You can
 * change this scope.
 *
 * @author Dongheng Zhan
 */
@Named(value = "contactApplication")
@ApplicationScoped

public class ContactApplication {

	@ManagedProperty(value = "#{contactManagedBean}")
    ContactManagedBean contactManagedBean;

    private ArrayList<ContactPerson> contacts;

    private boolean showForm = true;

    public boolean isShowForm() {
        return showForm;
    }
    
    public ContactApplication() throws Exception {
        contacts = new ArrayList<>();

        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        contactManagedBean = (ContactManagedBean) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(elContext, null, "contactManagedBean");

        //get properties from db 
        updateContactList();
    }
    public ContactPerson getCustomerByID(int contactID) {
    	ContactPerson contact = new ContactPerson();
    	for(ContactPerson con:getContacts()) {
    		if (con.getConactPersonId() == contactID) {
    			return con;
    		}
    	}
		return contact;
    }
    
    public ArrayList<ContactPerson> getContacts(int userId){
    	ArrayList<ContactPerson> cons = new ArrayList<ContactPerson>();
    	for(ContactPerson con:getContacts()) {
    		if (con.getUserId()== userId) {
    			cons.add(con);
    	}
    		}
    	return cons;
    }
    public ArrayList<ContactPerson> getContacts() {
        return contacts;
    }

    private void setContacts(ArrayList<ContactPerson> newContacts) {
        this.contacts = newContacts;
    }

    
    public void updateContactList() {
        if (contacts != null && contacts.size() > 0)
        {
            
        }
        else
        {
        	contacts.clear();

            for (fit5042.assignment.repository.entities.ContactPerson contact : contactManagedBean.getAllContacts())
            {
            	contacts.add(contact);
            }

            setContacts(contacts);
        }
    }
    
    public void searchContactById(int contactId) {
        contacts.clear();

        contacts.add(contactManagedBean.searchContactById(contactId));
    }

    public void searchContactByCustomerId(int customerId) {
        contacts.clear();
        Set<ContactPerson> contactsByCustomer = contactManagedBean.searchContactByCustomerId(customerId);
        for (ContactPerson contact : contactsByCustomer) {
            contacts.add(contact);
        }

    }
    public void searchPart(int userId)
    {
    	contacts.clear();
        
        for (fit5042.assignment.repository.entities.ContactPerson contact : contactManagedBean.getAllContacts())
        {
        	if(contact.getUserId() == userId) {
        	    contacts.add(contact);
        	}
        }
        
        setContacts(contacts);
    }
    
    public void searchAll()
    {
    	contacts.clear();
        
        for (fit5042.assignment.repository.entities.ContactPerson contact : contactManagedBean.getAllContacts())
        {
        	contacts.add(contact);
        }
        
        setContacts(contacts);
    }
}
