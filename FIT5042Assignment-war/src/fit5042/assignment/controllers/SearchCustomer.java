package fit5042.assignment.controllers;

import javax.el.ELContext;
import fit5042.assignment.repository.entities.*;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import fit5042.assignment.mbeans.CustomerManagedBean;
import javax.faces.bean.ManagedProperty;

/**
 *
 * @author Dongheng Zhan
 */
@RequestScoped
@Named("searchCustomer")
public class SearchCustomer {
    private boolean showForm = true;

    private Customer customer;

    CustomerApplication app;

    private int searchByInt;

    
    public CustomerApplication getApp() {
        return app;
    }

    public void setApp(CustomerApplication app) {
        this.app = app;
    }


    public int getSearchByInt() {
        return searchByInt;
    }

    public void setSearchByInt(int searchByInt) {
        this.searchByInt = searchByInt;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Customer getCustomer() {
        return customer;
    }

    public boolean isShowForm() {
        return showForm;
    }

    public SearchCustomer() {
        ELContext context
                = FacesContext.getCurrentInstance().getELContext();

        app = (CustomerApplication) FacesContext.getCurrentInstance()
                .getApplication()
                .getELResolver()
                .getValue(context, null, "customerApplication");

        app.updateCustomerList();

    }

    /**
     * Normally each page should have a backing bean but you can actually do it
     * any how you want.
     *
     * @param Customer Id
     */
    public void searchCustomerById(int customerId) {
        try {
            //search this Customer then refresh the list in CustomerApplication bean
            app.searchCustomerById(customerId);
        } catch (Exception ex) {

        }
        showForm = true;

    }

    public void searchPart(int userId) {
		try {
			// return all properties from db via EJB
			app.searchPart(userId);
		} catch (Exception ex) {

		}
		showForm = true;
	}
    
    public void searchAll() {
        try {
            //return all properties from db via EJB
            app.searchAll();
        } catch (Exception ex) {

        }
        showForm = true;
    }

}
