package fit5042.assignment.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.assignment.mbeans.GroupsManagedBean;
import fit5042.assignment.repository.entities.*;
import javax.faces.bean.ManagedProperty;

/**
*
* @author Dongheng Zhan
*/
@RequestScoped
@Named("removeGroups")
public class RemoveGroups {
	
	@ManagedProperty(value = "#{groupsManagedBean}")
	GroupsManagedBean groupsManagedBean;

	private boolean showForm = true;

	private Groups group;

	GroupApplication app;

	public void setGroup(Groups group) {
		this.group = group;
	}

	public Groups getGroup() {
		return group;
	}

	public boolean isShowForm() {
		return showForm;
	}

	public RemoveGroups() {
		ELContext context = FacesContext.getCurrentInstance().getELContext();

		app = (GroupApplication) FacesContext.getCurrentInstance().getApplication().getELResolver().getValue(context,
				null, "groupApplication");

		ELContext elContext = FacesContext.getCurrentInstance().getELContext();
		groupsManagedBean = (GroupsManagedBean) FacesContext.getCurrentInstance().getApplication().getELResolver()
				.getValue(elContext, null, "groupsManagedBean");
	}

	public void removeGroups(int groupId) {
		try {
			// remove this Customer from db via EJB
			groupsManagedBean.removeGroups(groupId);

			// refresh the list in CustomerApplication bean
			app.searchAll();

			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage("Group has been deleted succesfully"));
		} catch (Exception ex) {

		}
		showForm = true;

	}

}
