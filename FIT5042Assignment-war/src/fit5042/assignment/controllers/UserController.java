package fit5042.assignment.controllers;

import javax.el.ELContext;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;

/**
 *
 * 
 * @author Dongheng Zhan
 */
@Named(value = "userController")
@RequestScoped
public class UserController {
	private int userId;
    private String userName;
    
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	private fit5042.assignment.repository.entities.Users user;
	
	 public UserController() {
	    
	    	userId = Integer.valueOf(FacesContext.getCurrentInstance()
	                .getExternalContext()
	                .getRequestParameterMap()
	                .get("userID"));
	    	user = getUser();
	    }
	 
	 public fit5042.assignment.repository.entities.Users getUser() {
	        if (user == null) {
	             
	            ELContext context
	                    = FacesContext.getCurrentInstance().getELContext();
	            UserApplication app
	                    = (UserApplication) FacesContext.getCurrentInstance()
	                            .getApplication()
	                            .getELResolver()
	                            .getValue(context, null, "userApplication");
	           
	            return app.getUserByID(userId);
	        }
	        return user;
	    }
	 
}
