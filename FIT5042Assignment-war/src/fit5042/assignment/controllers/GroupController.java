package fit5042.assignment.controllers;

import javax.el.ELContext;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;

/**
 *
 * 
 * @author Dongheng Zhan
 */
@Named(value = "groupController")
@RequestScoped
public class GroupController {
	private int groupId;

	public int getGroupId() {
		return groupId;
	}

	public void setUserId(int groupId) {
		this.groupId = groupId;
	}

	private fit5042.assignment.repository.entities.Groups groups;
	
	 public GroupController() {
	    
	    	groupId = Integer.valueOf(FacesContext.getCurrentInstance()
	                .getExternalContext()
	                .getRequestParameterMap()
	                .get("groupID"));
	       
	    	groups = getGroup();
	    }
	 
	 public fit5042.assignment.repository.entities.Groups getGroup() {
	        if (groups == null) {
	             
	            ELContext context
	                    = FacesContext.getCurrentInstance().getELContext();
	            GroupApplication app
	                    = (GroupApplication) FacesContext.getCurrentInstance()
	                            .getApplication()
	                            .getELResolver()
	                            .getValue(context, null, "groupApplication");
	           
	            return app.getGroupByID(groupId);
	        }
	        return groups;
	    }

}
