package fit5042.assignment.controllers;

import java.util.ArrayList;
import java.util.Set;

import javax.enterprise.context.ApplicationScoped;

import fit5042.assignment.mbeans.GroupsManagedBean;

import javax.inject.Named;

import fit5042.assignment.repository.entities.Groups;

import javax.el.ELContext;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;

/**
 * The class is a demonstration of how the application scope works. You can
 * change this scope.
 *
 * @author Dongheng Zhan
 */
@Named(value = "groupApplication")
@ApplicationScoped
public class GroupApplication {
	
	@ManagedProperty(value = "#{groupsManagedBean}")
    GroupsManagedBean groupsManagedBean;

    private ArrayList<Groups> groups;

    private boolean showForm = true;

    public boolean isShowForm() {
        return showForm;
    }
    
    public Groups getGroupByID(int groupID) {
    	Groups groups = new Groups();
    	for(Groups gp:getGroups()) {
    		if (gp.getGroupId() == groupID) {
    			return gp;
    		}
    	}
		return groups;
    }
    
    public GroupApplication() throws Exception {
    	groups = new ArrayList<>();

        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        groupsManagedBean = (GroupsManagedBean) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(elContext, null, "groupsManagedBean");

        //get properties from db 
        updateGroupList();
    }
    public ArrayList<Groups> getGroups() {
        return groups;
    }

    private void setGroups(ArrayList<Groups> newGroups) {
    	
        this.groups = newGroups;
    }

    
    public void updateGroupList() {
        if (groups != null && groups.size() > 0)
        {
            
        }
        else
        {
        	groups.clear();

            for (fit5042.assignment.repository.entities.Groups group : groupsManagedBean.getAllGroups())
            {
            	groups.add(group);
            }

            setGroups(groups);
        }
    }
    
    public void searchGroupsById(int groupId) {
        groups.clear();

       groups.add(groupsManagedBean.searchGroupsById(groupId));
    }
    
    public void searchAll()
    {
    	groups.clear();
        
        for (fit5042.assignment.repository.entities.Groups group : groupsManagedBean.getAllGroups())
        {
        	groups.add(group);
        }
        
        setGroups(groups);
    }

}
