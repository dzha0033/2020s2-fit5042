package fit5042.assignment.controllers;

import fit5042.assignment.repository.entities.TypeOfIndustry;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@RequestScoped
@Named(value = "type")
public class Type implements Serializable{
	private int typeId;
	private String typeOfIndustry;
	private Set<String> tags;
	
	public Type() {
		this.tags = new HashSet<>();
	}
	
	
	public int getTypeId() {
		return typeId;
	}
	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}
	public String getTypeOfIndustry() {
		return typeOfIndustry;
	}
	public void setTypeOfIndustry(String typeOfIndustry) {
		this.typeOfIndustry = typeOfIndustry;
	}
	
	public Set<String> getTags() {
		return tags;
	}

	public void setTags(Set<String> tags) {
		this.tags = tags;
	}
	
}
