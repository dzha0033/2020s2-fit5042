package fit5042.assignment.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.assignment.mbeans.UsersManagedBean;
import fit5042.assignment.repository.entities.*;
import javax.faces.bean.ManagedProperty;

/**
*
* @author Dongheng Zhan
*/
@RequestScoped
@Named("removeUsers")
public class RemoveUsers {
	
	@ManagedProperty(value = "#{usersManagedBean}")
	UsersManagedBean usersManagedBean;

	private boolean showForm = true;

	private Users user;

	UserApplication app;

	public void setUser(Users user) {
		this.user = user;
	}

	public Users getUser() {
		return user;
	}

	public boolean isShowForm() {
		return showForm;
	}

	public RemoveUsers() {
		ELContext context = FacesContext.getCurrentInstance().getELContext();

		app = (UserApplication) FacesContext.getCurrentInstance().getApplication().getELResolver().getValue(context,
				null, "userApplication");

		ELContext elContext = FacesContext.getCurrentInstance().getELContext();
		usersManagedBean = (UsersManagedBean) FacesContext.getCurrentInstance().getApplication().getELResolver()
				.getValue(elContext, null, "usersManagedBean");
	}

	public void removeUsers(int userId) {
		try {
			// remove this Customer from db via EJB
			usersManagedBean.removeUser(userId);

			// refresh the list in CustomerApplication bean
			app.searchAll();

			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage("User has been deleted succesfully"));
		} catch (Exception ex) {

		}
		showForm = true;

	}

}
