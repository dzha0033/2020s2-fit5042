package fit5042.assignment.controllers;

import fit5042.assignment.repository.entities.Address;
import fit5042.assignment.repository.entities.ContactPerson;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Range;

/**
*
* @author Dongheng Zhan
*/
@RequestScoped
@Named(value = "customer")
public class Customer implements Serializable {
	private int customerId;
	@Size(min = 3,max = 9)
	private String name;
	private String typeOfIndustry;
	private String email;
	
	@Range(max=99999)
	private int numberOfOrders;
	
	private String userType;
    private int userId;
    
	private Address address;
    private Set<ContactPerson> contactPersons;
    
	private Set<String> tags;
	
	private String streetNumber;
    private String streetAddress;
    private String suburb;
    private String postcode;
    private String state;

    private int conactPersonId;
	private String contactName;
	private String gender;
	private String contactEmail;
	private String phoneNumber;
	
	private Set<fit5042.assignment.repository.entities.Customer> customers;
	
	
	public Customer() {
	        this.tags = new HashSet<>();
	    }
	
	public Customer(int customerId, Address address, int numberOfOrders, String name, String typeOfIndustry,String email,
			String userType,int userId, Set<String> tags) {
		this.userId = userId;
		this.customerId = customerId;
		this.address = address;
		this.name = name;
		this.typeOfIndustry = typeOfIndustry;
		this.email = email;
		this.userType = userType;
		this.numberOfOrders = numberOfOrders;
		this.contactPersons = new HashSet<>();
		this.tags = tags;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTypeOfIndustry() {
		return typeOfIndustry;
	}

	public void setTypeOfIndustry(String typeOfIndustry) {
		this.typeOfIndustry = typeOfIndustry;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getNumberOfOrders() {
		return numberOfOrders;
	}

	public void setNumberOfOrders(int numberOfOrders) {
		this.numberOfOrders = numberOfOrders;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public Set<String> getTags() {
		return tags;
	}

	public void setTags(Set<String> tags) {
		this.tags = tags;
	}

	public String getStreetNumber() {
		return streetNumber;
	}

	public void setStreetNumber(String streetNumber) {
		this.streetNumber = streetNumber;
	}

	public String getStreetAddress() {
		return streetAddress;
	}

	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	public String getSuburb() {
		return suburb;
	}

	public void setSuburb(String suburb) {
		this.suburb = suburb;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public int getConactPersonId() {
		return conactPersonId;
	}

	public void setConactPersonId(int conactPersonId) {
		this.conactPersonId = conactPersonId;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Set<ContactPerson> getContactPersons() {
		return contactPersons;
	}

	public void setContactPersons(Set<ContactPerson> contactPersons) {
		this.contactPersons = contactPersons;
	}

	public Set<fit5042.assignment.repository.entities.Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(Set<fit5042.assignment.repository.entities.Customer> customers) {
		this.customers = customers;
	}
	
	@Override
	public String toString() {
		return "Customer [customerId=" + customerId + ", name=" + name + ", typeOfIndustry=" + typeOfIndustry
				+ ", email=" + email + ", numberOfOrders=" + numberOfOrders + ", userType=" + userType + ", address="
				+ address + ", contactPerson=" + contactPersons + ", tags=" + tags + "]";
	}
	
}
