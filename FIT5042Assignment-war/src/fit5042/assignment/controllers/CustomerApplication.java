package fit5042.assignment.controllers;

import java.util.ArrayList;
import javax.enterprise.context.ApplicationScoped;
import fit5042.assignment.mbeans.CustomerManagedBean;
import javax.inject.Named;

import fit5042.assignment.repository.entities.ContactPerson;
import fit5042.assignment.repository.entities.Customer;
import javax.el.ELContext;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;

/**
 * The class is a demonstration of how the application scope works. You can
 * change this scope.
 *
 * @author Dongheng Zhan
 */
@Named(value = "customerApplication")
@ApplicationScoped

public class CustomerApplication {

    //dependency injection of managed bean here so that we can use its methods
    @ManagedProperty(value = "#{customerManagedBean}")
    CustomerManagedBean customerManagedBean;

    private ArrayList<Customer> customers;

    private boolean showForm = true;

    public boolean isShowForm() {
        return showForm;
    }

    // Add some customer data from db to app 
    public CustomerApplication() throws Exception {
        customers = new ArrayList<>();

        //instantiate customerManagedBean
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        customerManagedBean = (CustomerManagedBean) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(elContext, null, "customerManagedBean");

        //get properties from db 
        updateCustomerList();
    }

    
    public Customer getCustomerByID(int customerID) {
    	Customer customer = new Customer();
    	for(Customer cus:getCustomers()) {
    		if (cus.getCustomerId() == customerID) {
    			return cus;
    		}
    	}
		return customer;
    }
    
    public int setID() {
    	 int i = 1;
         int k = 0;
         for(;i<=getCustomers().get(0).getCustomerId();i++) {
         	k=0;
         	for(Customer cus : getCustomers())
         	{
         		if (cus.getCustomerId() == i)
         		{
         			k++;
         		}
         	}
         	if (k == 0 ) {
         		return i;
         	}
         }
         return i+1;
    }
    
    public ArrayList<Customer> getCustomers(int userId){
    	ArrayList<Customer> cust = new ArrayList<Customer>();
    	for(Customer cus:getCustomers()) {
    		if (cus.getUserId()== userId) {
    			cust.add(cus);
    	}
    		}
    	return cust;
    }
    
    public Boolean checkEmail (String email) {
    	for(Customer cus:getCustomers()) {
    		if (cus.getEmail()== email) {
    			return false;
    		}
    	}
    	return true;
    }
    
    public ArrayList<Customer> getCustomers() {
        return customers;
    }

    private void setCustomers(ArrayList<Customer> newProperties) {
        this.customers = newProperties;
    }

    //when loading, and after adding or deleting, the customer list needs to be refreshed
    //this method is for that purpose
    public void updateCustomerList() {
        if (customers != null && customers.size() > 0)
        {
            
        }
        else
        {
        	customers.clear();

            for (fit5042.assignment.repository.entities.Customer customer : customerManagedBean.getAllCustomers())
            {
                customers.add(customer);
            }

            setCustomers(customers);
        }
    }

    public void searchCustomerById(int customerId) {
        customers.clear();

        customers.add(customerManagedBean.searchCustomerById(customerId));
    }

    public void searchPart(int userId)
    {
    	customers.clear();
        
        for (fit5042.assignment.repository.entities.Customer customer : customerManagedBean.getAllCustomers())
        {
        	if(customer.getUserId() == userId) {
        	    customers.add(customer);
        	}
        }
        
        setCustomers(customers);
    }
    
    public void searchAll()
    {
    	customers.clear();
        
        for (fit5042.assignment.repository.entities.Customer customer : customerManagedBean.getAllCustomers())
        {
        	customers.add(customer);
        }
        
        setCustomers(customers);
    }
}
