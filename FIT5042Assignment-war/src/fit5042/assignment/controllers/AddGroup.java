package fit5042.assignment.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import fit5042.assignment.mbeans.GroupsManagedBean;

import javax.faces.bean.ManagedProperty;

/**
*
* @author Dongheng Zhan
*/
@RequestScoped
@Named("addGroup")
public class AddGroup {
	@ManagedProperty(value = "#{groupsManagedBean}")
	GroupsManagedBean groupsManagedBean;

    private boolean showForm = true;

    private Groups group;

    GroupApplication app;

    public void setGroup(Groups group) {
        this.group = group;
    }

    public Groups getGroup() {
        return group;
    }

    public boolean isShowForm() {
        return showForm;
    }

    public AddGroup() {
        ELContext context
                = FacesContext.getCurrentInstance().getELContext();

        app = (GroupApplication) FacesContext.getCurrentInstance()
                .getApplication()
                .getELResolver()
                .getValue(context, null, "groupApplication");

        //instantiate CustomerManagedBean
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        groupsManagedBean = (GroupsManagedBean) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(elContext, null, "groupsManagedBean");
    }

    public void addGroup(Groups localGroup) {
        //this is the local Customer, not the entity
        try {
            //add this Customer to db via EJB
             groupsManagedBean.addGroup(localGroup);;

            //refresh the list in CustomerApplication bean
             app.searchAll();

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Group has been added succesfully"));
        } catch (Exception ex) {

        }
        showForm = true;
    }


}
