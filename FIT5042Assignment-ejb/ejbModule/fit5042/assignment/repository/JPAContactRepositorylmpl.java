package fit5042.assignment.repository;

import fit5042.assignment.repository.entities.ContactPerson;
import fit5042.assignment.repository.entities.Customer;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
*
* @author Dongheng Zhan
*/
@Stateless
public class JPAContactRepositorylmpl implements ContactRepository{
	@PersistenceContext(name="FIT5042Assignment-ejbPU")
    private EntityManager entityManager;

    @Override
    public void addContact(ContactPerson contact) throws Exception {
        List<ContactPerson> contacts = entityManager.createNamedQuery(ContactPerson.GET_ALL_QUERY_NAME).getResultList();
        int i = 1;
        int k = 0;
        for(;i<=contacts.size();i++) {
        	k=0;
        	for(ContactPerson con : contacts)
        	{
        		if (con.getConactPersonId() == i)
        		{
        			k++;
        		}
        	}
        	if (k == 0 ) {
        		contact.setConactPersonId(i);
        		entityManager.persist(contact);
        		return;
        	}
        	if(i == contacts.get(0).getConactPersonId()){
        		 contact.setConactPersonId(contacts.get(0).getConactPersonId() + 1);
        	     entityManager.persist(contact);
        	}
        }
      
    }
    
    @Override
    public ContactPerson searchContactById(int id) throws Exception {
        ContactPerson contact = entityManager.find(ContactPerson.class, id);
        contact.getTags();
        return contact;
    }
    
    @Override
    public ContactPerson searchContactByCustomerId(int customerId) throws Exception {
        ContactPerson contact = entityManager.find(ContactPerson.class, customerId);
        contact.getTags();
        return contact;
    }
    
    @Override
    public List<ContactPerson> getAllContacts() throws Exception {
        return entityManager.createNamedQuery(ContactPerson.GET_ALL_QUERY_NAME).getResultList();
    }
    
    @Override
    public List<Customer> getAllCustomers() throws Exception {
        return entityManager.createNamedQuery(Customer.GET_ALL_QUERY_NAME).getResultList();
    }


    @Override
    public void removeContact(int contactId) throws Exception {
        //complete this method
    	try {
            entityManager.remove(searchContactById(contactId));
        } catch (Exception ex) {

        }
    }

    @Override
    public void editContact(ContactPerson contact) throws Exception {
        try {
            entityManager.merge(contact);
        } catch (Exception ex) {

        }
    }
    
    @Override
    public Set<ContactPerson> searchContactByCustomer(Customer customer) throws Exception {
        customer = entityManager.find(Customer.class, customer.getCustomerId());
        customer.getContactPerson().size();
        entityManager.refresh(customer);

        return customer.getContactPerson();
    }
}
