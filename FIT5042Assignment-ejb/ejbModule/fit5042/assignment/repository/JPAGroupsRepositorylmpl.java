package fit5042.assignment.repository;

import fit5042.assignment.repository.entities.ContactPerson;
import fit5042.assignment.repository.entities.Groups;

import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Dongheng Zhan
 */
@Stateless
public class JPAGroupsRepositorylmpl implements GroupRepository{
	
	@PersistenceContext(name="FIT5042Assignment-ejbPU")
    private EntityManager entityManager;
	
	 @Override
	    public void addGroup(Groups group) throws Exception {
	        List<Groups> groups = entityManager.createNamedQuery(Groups.GET_ALL_QUERY_NAME).getResultList();
	        int i = 1;
	        int k = 0;
	        for(;i<=groups.size();i++) {
	        	k=0;
	        	for(Groups gp : groups)
	        	{
	        		if (gp.getGroupId() == i)
	        		{
	        			k++;
	        		}
	        	}
	        	if (k == 0 ) {
	        		group.setGroupId(i);
	        		entityManager.persist(group);
	        		return;
	        	}
	        	if(i == groups.get(0).getGroupId()){
	        		group.setGroupId(groups.get(0).getGroupId() + 1);
	    	        entityManager.persist(group);
	        	}
	        }
	    }
	 
	 @Override
	    public Groups searchGroupsById(int id) throws Exception {
		 Groups group = entityManager.find(Groups.class, id);
		  group.getTags();
	        return group;
	    }
	 
	 @Override
	    public List<Groups> getAllGroups() throws Exception {
		 
	        return entityManager.createNamedQuery(Groups.GET_ALL_QUERY_NAME).getResultList();
	    }
	 
	 @Override
	    public void removeGroups(int groupId) throws Exception {
	        //complete this method
	    	try {
	            entityManager.remove(searchGroupsById(groupId));
	        } catch (Exception ex) {

	        }
	    }
	 
	 @Override
	    public void editGroups(Groups group) throws Exception {
	        try {
	            entityManager.merge(group);
	        } catch (Exception ex) {

	        }
	    }

}
