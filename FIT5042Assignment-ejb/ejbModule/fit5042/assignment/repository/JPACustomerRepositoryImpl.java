package fit5042.assignment.repository;

import fit5042.assignment.repository.entities.ContactPerson;
import fit5042.assignment.repository.entities.Customer;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Dongheng Zhan
 */
@Stateless
public class JPACustomerRepositoryImpl implements CustomerRepository {

    //insert code (annotation) here to use container managed entity manager to complete these methods
	@PersistenceContext(name="FIT5042Assignment-ejbPU")
    private EntityManager entityManager;

    @Override
    public void addCustomer(Customer customer) throws Exception {
        List<Customer> customers = entityManager.createNamedQuery(Customer.GET_ALL_QUERY_NAME).getResultList();
        int i = 1;
        int k = 0;
        for(;i<=customers.size();i++) {
        	k=0;
        	for(Customer cus : customers)
        	{
        		if (cus.getCustomerId() == i)
        		{
        			k++;
        		}
        	}
        	if (k == 0 ) {
        		customer.setCustomerId(i);
        		entityManager.persist(customer);
        		return;
        	}
        	if(i == customers.get(0).getCustomerId()){
        	 customer.setCustomerId(customers.get(0).getCustomerId() + 1);
        	 entityManager.persist(customer);
        	}
        }
       
    }
    
    @Override
    public Customer searchCustomerById(int id) throws Exception {
        Customer customer = entityManager.find(Customer.class, id);
        customer.getTags();
        return customer;
    }

    @Override
    public List<Customer> getAllCustomers() throws Exception {
        return entityManager.createNamedQuery(Customer.GET_ALL_QUERY_NAME).getResultList();
    }

    @Override
    public List<ContactPerson> getAllContactPeople() throws Exception {
        return entityManager.createNamedQuery(ContactPerson.GET_ALL_QUERY_NAME).getResultList();
    }

    @Override
    public void removeCustomer(int customerId) throws Exception {
        //complete this method
    	try {
            entityManager.remove(searchCustomerById(customerId));
        } catch (Exception ex) {

        }
    }

    @Override
    public void editCustomer(Customer customer) throws Exception {
        try {
            entityManager.merge(customer);
        } catch (Exception ex) {

        }
    }
}
