package fit5042.assignment.repository;

import fit5042.assignment.repository.entities.Groups;
import fit5042.assignment.repository.entities.TypeOfIndustry;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Dongheng Zhan
 */
@Stateless
public class JPATypeRepositorylmpl implements TypeOfIndustryRepository{
	
	@PersistenceContext(name="FIT5042Assignment-ejbPU")
    private EntityManager entityManager;
	
	 @Override
	    public void addType(TypeOfIndustry type) throws Exception {
	        List<TypeOfIndustry> types = entityManager.createNamedQuery(TypeOfIndustry.GET_ALL_QUERY_NAME).getResultList();
	        int i = 1;
	        int k = 0;
	        for(;i<=types.size();i++) {
	        	k=0;
	        	for(TypeOfIndustry tp : types)
	        	{
	        		if (tp.getTypeId() == i)
	        		{
	        			k++;
	        		}
	        	}
	        	if (k == 0 ) {
	        		type.setTypeId(i);
	        		entityManager.persist(type);
	        		return;
	        	}
	        	if(i == types.get(0).getTypeId()){
	        		type.setTypeId(types.get(0).getTypeId() + 1);
	    	        entityManager.persist(type);
	        	}
	        }
	    }
	 
	 @Override
	    public TypeOfIndustry searchTypeById(int id) throws Exception {
	        TypeOfIndustry type = entityManager.find(TypeOfIndustry.class, id);
	        type.getTags();
	        return type;
	    }
	 
	 @Override
	    public List<TypeOfIndustry> getAllTypes() throws Exception {
		 
	        return entityManager.createNamedQuery(TypeOfIndustry.GET_ALL_QUERY_NAME).getResultList();
	    }
	 
	 @Override
	    public void removeType(int typeId) throws Exception {
	        //complete this method
	    	try {
	            entityManager.remove(searchTypeById(typeId));
	        } catch (Exception ex) {

	        }
	    }
	 
	 @Override
	    public void editType(TypeOfIndustry type) throws Exception {
	        try {
	            entityManager.merge(type);
	        } catch (Exception ex) {

	        }
	    }
}
