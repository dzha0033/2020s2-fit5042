package fit5042.assignment.repository;

import fit5042.assignment.repository.entities.TypeOfIndustry;
import fit5042.assignment.repository.entities.Users;

import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Dongheng Zhan
 */
@Stateless
public class JPAUsersRepositorylmpl implements UserRepository{
	
	@PersistenceContext(name="FIT5042Assignment-ejbPU")
    private EntityManager entityManager;
	
	 @Override
	    public void addUser(Users user) throws Exception {
	        List<Users> users = entityManager.createNamedQuery(Users.GET_ALL_QUERY_NAME).getResultList();
	        int i = 1;
	        int k = 0;
	        for(;i<=users.size();i++) {
	        	k=0;
	        	for(Users us : users)
	        	{
	        		if (us.getUserId() == i)
	        		{
	        			k++;
	        		}
	        	}
	        	if (k == 0 ) {
	        		user.setUserId(i);
	        		entityManager.persist(user);
	        		return;
	        	}
	        	if(i == users.get(0).getUserId()){
	        		user.setUserId(users.get(0).getUserId() + 1);
	    	        entityManager.persist(user);
	        	}
	        }
	    }
	 
	 @Override
	    public Users searchUsersById(int id) throws Exception {
	        Users user = entityManager.find(Users.class, id);
	        user.getTags();
	        return user;
	    }
	 
	 @Override
	    public List<Users> getAllUsers() throws Exception {
		 
	        return entityManager.createNamedQuery(Users.GET_ALL_QUERY_NAME).getResultList();
	    }
	 
	 @Override
	    public void removeUser(int userId) throws Exception {
	        //complete this method
	    	try {
	            entityManager.remove(searchUsersById(userId));
	        } catch (Exception ex) {

	        }
	    }
	 
	 @Override
	    public void editUsers(Users user) throws Exception {
	        try {
	            entityManager.merge(user);
	        } catch (Exception ex) {

	        }
	    }

}
