package fit5042.assignment.repository;

import java.util.List;
import java.util.Set;
import javax.ejb.Remote;

import fit5042.assignment.repository.entities.TypeOfIndustry;

/**
 * @author Dongheng Zhan
 */
@Remote
public interface TypeOfIndustryRepository {
	
	public void addType(TypeOfIndustry type) throws Exception;
	
	public List<TypeOfIndustry> getAllTypes() throws Exception;

	public void removeType(int typeId) throws Exception;
	
	public TypeOfIndustry searchTypeById(int id) throws Exception;

    public void editType(TypeOfIndustry type) throws Exception;
}
