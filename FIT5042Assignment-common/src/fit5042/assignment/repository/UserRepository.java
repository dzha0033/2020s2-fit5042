package fit5042.assignment.repository;

import java.util.List;
import java.util.Set;
import javax.ejb.Remote;

import fit5042.assignment.repository.entities.Users;

/**
 * @author Dongheng Zhan
 */
@Remote
public interface UserRepository {

    public void addUser(Users user) throws Exception;
	
	public List<Users> getAllUsers() throws Exception;

	public void removeUser(int userId) throws Exception;
	
	public Users searchUsersById(int id) throws Exception;

    public void editUsers(Users user) throws Exception;

}
