package fit5042.assignment.repository;

import java.util.List;
import java.util.Set;
import javax.ejb.Remote;

import fit5042.assignment.repository.entities.ContactPerson;
import fit5042.assignment.repository.entities.Customer;

/**
 * @author Dongheng Zhan
 */
@Remote
public interface CustomerRepository {

    
    public void addCustomer(Customer customer) throws Exception;

   
    public Customer searchCustomerById(int id) throws Exception;

  
    public List<Customer> getAllCustomers() throws Exception;

   
    public List<ContactPerson> getAllContactPeople() throws Exception;

   
    public void removeCustomer(int customerId) throws Exception;

    public void editCustomer(Customer customer) throws Exception;


}
