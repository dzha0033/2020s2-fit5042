package fit5042.assignment.repository.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;



/**
 *
 * @author Dongheng Zhan
 */
@Entity
@Table(name = "CUSTOMER")
@NamedQueries({
		@NamedQuery(name = Customer.GET_ALL_QUERY_NAME, query = "SELECT c FROM Customer c order by c.customerId desc") })

public class Customer implements Serializable {

	public static final String GET_ALL_QUERY_NAME = "Customer.getAll";

	private int customerId;
	private String name;
	private String typeOfIndustry;
	private String email;
	private int numberOfOrders;
	private String userType;
	private int userId;
	
	private Address address;
	private Set<ContactPerson> contactPersons;

	private Set<String> tags;

	public Customer() {
		this.tags = new HashSet<>();
	}

	public Customer(int customerId, Address address, int numberOfOrders, String name, String typeOfIndustry,String email,
			String userType,int userId, Set<String> tags) {
		this.userId = userId;
		this.customerId = customerId;
		this.address = address;
		this.name = name;
		this.typeOfIndustry = typeOfIndustry;
		this.email = email;
		this.userType = userType;
		this.numberOfOrders = numberOfOrders;
		this.contactPersons = new HashSet<>();
		this.tags = tags;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	@Id
	@Column(name = "customer_id")
	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	// insert annotation here to make address as embeded to Property entity and
	// stored as part of Property
	@Embedded
	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "number_of_Orders")
	public int getNumberOfOrders() {
		return numberOfOrders;
	}

	public void setNumberOfOrders(int numberOfOrders) {
		this.numberOfOrders = numberOfOrders;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTypeOfIndustry() {
		return typeOfIndustry;
	}

	public void setTypeOfIndustry(String typeOfIndustry) {
		this.typeOfIndustry = typeOfIndustry;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

    @OneToMany(mappedBy ="customer",cascade = CascadeType.MERGE,fetch=FetchType.EAGER)
	public Set<ContactPerson> getContactPerson() {
		return contactPersons;
	}

	public void setContactPerson(Set<ContactPerson> contactPersons) {
		this.contactPersons = contactPersons;
	}
	
	@Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + this.customerId;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Customer other = (Customer) obj;
        if (this.customerId != other.customerId) {
            return false;
        }
        return true;
    }

	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "TAG")
	@Column(name = "VALUE")
	public Set<String> getTags() {
		return tags;
	}

	public void setTags(Set<String> tags) {
		this.tags = tags;
	}

	@Override
	public String toString() {
		return  customerId + "-" + name + "-" + typeOfIndustry ;
	}

	
}
