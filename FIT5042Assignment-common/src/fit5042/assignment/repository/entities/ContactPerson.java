package fit5042.assignment.repository.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Dongheng Zhan
 */
@Entity
@Table(name = "CONTACT_PERSON")
@NamedQueries({ @NamedQuery(name = ContactPerson.GET_ALL_QUERY_NAME, query = "SELECT c FROM ContactPerson c order by c.conactPersonId desc") })
public class ContactPerson implements Serializable {

	public static final String GET_ALL_QUERY_NAME = "ContactPerson.getAll";

	private int conactPersonId;
	private String name;
	private String gender;
	private Address address;
	private String email;
	private String phoneNumber;
    private int userId;
    
	private Customer customer;
	
	private Set<String> tags;
	
	public ContactPerson() {
		this.tags = new HashSet<>();
	}

	public ContactPerson(int conactPersonId, String name, String phoneNumber,String gender, String email,int userId,Address address,Customer customer, Set<String> tags) {
		this.userId = userId;
		this.conactPersonId = conactPersonId;
		this.name = name;
		this.gender = gender;
		this.address = address;
		this.email = email;
		this.phoneNumber = phoneNumber;
		this.customer = customer;
		this.tags = tags;
	}


	@Id
	@Column(name = "contact_person_id")
	public int getConactPersonId() {
		return conactPersonId;
	}

	public void setConactPersonId(int conactPersonId) {
		this.conactPersonId = conactPersonId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "phone_number")
	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@Column(name = "gender")
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	@Embedded
	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn
	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 53 * hash + this.conactPersonId;
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final ContactPerson other = (ContactPerson) obj;
		if (this.conactPersonId != other.conactPersonId) {
			return false;
		}
		return true;
	}

	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "TAG")
	@Column(name = "VALUE")
	public Set<String> getTags() {
		return tags;
	}

	public void setTags(Set<String> tags) {
		this.tags = tags;
	}
	
	@Override
	public String toString() {
		return "Conact Person Id :" + conactPersonId + ", name :" + name + ", gender :" + gender
				+ ", address :" + address + ", email :" + email + ", phoneNumber :" + phoneNumber ;
	}
}
