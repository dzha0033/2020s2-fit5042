package fit5042.assignment.repository.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;



/**
 *
 * @author Dongheng Zhan
 */
@Entity
@Table(name = "USERS")
@NamedQueries({
		@NamedQuery(name = Users.GET_ALL_QUERY_NAME, query = "SELECT u FROM Users u order by u.userId desc") })

public class Users implements Serializable{
	
public static final String GET_ALL_QUERY_NAME = "Users.getAll";
	
	private int userId;
	private String userName;
	private String password;
	private Set<String> tags;
	
	
	public Users() {
		this.tags = new HashSet<>();
	}
	
	public Users(int userId,String userName, String password,Set<String> tags) {
		this.userId = userId;
		this.userName  = userName;
		this.password = password;
		this.tags = tags;	
	}

	@Id
	@Column(name = "user_id")
	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	

	@Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + this.userId;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Users other = (Users) obj;
        if (this.userId != other.userId) {
            return false;
        }
        return true;
    }

	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "TAG")
	@Column(name = "VALUE")
	public Set<String> getTags() {
		return tags;
	}

	public void setTags(Set<String> tags) {
		this.tags = tags;
	}

	@Override
	public String toString() {
		return  userName;
	}
}
