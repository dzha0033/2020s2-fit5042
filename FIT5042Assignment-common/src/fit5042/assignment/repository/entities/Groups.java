package fit5042.assignment.repository.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
*
* @author Dongheng Zhan
*/
@Entity
@Table(name = "GROUPS")
@NamedQueries({
		@NamedQuery(name = Groups.GET_ALL_QUERY_NAME, query = "SELECT g FROM Groups g order by g.groupId desc") })

public class Groups implements Serializable{
	
public static final String GET_ALL_QUERY_NAME = "Groups.getAll";
	
	private int groupId;
	private String userName;
	private String groupName;
	private Set<String> tags;
	
	
	public Groups() {
		this.tags = new HashSet<>();
	}
	
	public Groups(int groupId,String userName, String groupName,Set<String> tags) {
		this.groupId = groupId;
		this.userName  = userName;
		this.groupName = groupName;
		this.tags = tags;	
	}

	@Id
	@Column(name = "group_id")
	public int getGroupId() {
		return groupId;
	}

	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	
	@Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + this.groupId;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Groups other = (Groups) obj;
        if (this.groupId != other.groupId) {
            return false;
        }
        return true;
    }

	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "TAG")
	@Column(name = "VALUE")
	public Set<String> getTags() {
		return tags;
	}

	public void setTags(Set<String> tags) {
		this.tags = tags;
	}

}
