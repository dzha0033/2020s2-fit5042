package fit5042.assignment.repository.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;



/**
 *
 * @author Dongheng Zhan
 */
@Entity
@Table(name = "TYPEOFINDUSTRY")
@NamedQueries({
		@NamedQuery(name = TypeOfIndustry.GET_ALL_QUERY_NAME, query = "SELECT t FROM TypeOfIndustry t order by t.typeId desc") })

public class TypeOfIndustry implements Serializable  {
	
	public static final String GET_ALL_QUERY_NAME = "TypeOfIndustry.getAll";
	
	private int typeId;
	private String type;
	private Set<String> tags;
	
	public TypeOfIndustry() {
		this.tags = new HashSet<>();
	}
	
	public TypeOfIndustry(int typeId,String type, Set<String> tags) {
		this.typeId = typeId;
		this.type  = type;
		this.tags = tags;	
	}
	
	@Id
	@Column(name = "type_id")
	public int getTypeId() {
		return typeId;
	}


	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}


	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	@Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + this.typeId;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TypeOfIndustry other = (TypeOfIndustry) obj;
        if (this.typeId != other.typeId) {
            return false;
        }
        return true;
    }

	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "TAG")
	@Column(name = "VALUE")
	public Set<String> getTags() {
		return tags;
	}

	public void setTags(Set<String> tags) {
		this.tags = tags;
	}

}
