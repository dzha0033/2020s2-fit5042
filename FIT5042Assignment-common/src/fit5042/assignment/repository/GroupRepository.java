package fit5042.assignment.repository;

import java.util.List;
import javax.ejb.Remote;

import fit5042.assignment.repository.entities.Groups;

@Remote
public interface GroupRepository {
	
	 public void addGroup(Groups groups) throws Exception;
		
		public List<Groups> getAllGroups() throws Exception;

		public void removeGroups(int groupId) throws Exception;
		
		public Groups searchGroupsById(int id) throws Exception;

	    public void editGroups(Groups groups) throws Exception;
}
