package fit5042.assignment.repository;

import java.util.List;
import java.util.Set;
import javax.ejb.Remote;

import fit5042.assignment.repository.entities.ContactPerson;
import fit5042.assignment.repository.entities.Customer;

/**
 * @author Dongheng Zhan
 */
@Remote
public interface ContactRepository {
	
	 public void addContact(ContactPerson contact) throws Exception;
	 
	 public ContactPerson searchContactById(int id) throws Exception;
	 
	 public ContactPerson searchContactByCustomerId(int customerId) throws Exception;
	 
	 public void removeContact(int contactId) throws Exception;

	 public void editContact(ContactPerson contact) throws Exception;

	 public List<ContactPerson> getAllContacts() throws Exception;
	 
	 public List<Customer> getAllCustomers() throws Exception;
	 
	 Set<ContactPerson> searchContactByCustomer(Customer customer) throws Exception;
}
